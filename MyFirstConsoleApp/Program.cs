﻿using System;

namespace MyFirstConsoleApp {
    class Program {
        static void Main(string[] args) {
            bool cont = true;
            do
            {
                Console.WriteLine("Hello World!");
                string inputHeight;
                string inputLength;
                Console.WriteLine("Enter height: ");
                inputHeight = Console.ReadLine();
                Console.WriteLine("Enter length: ");
                inputLength = Console.ReadLine();

                int height = Convert.ToInt32(inputHeight);
                int length = Convert.ToInt32(inputLength);



                Console.WriteLine(PrintSquare(height, length));

                Console.WriteLine("Draw more squares? 1 for more, 0 to end");
                string answer = Console.ReadLine();
                if(answer.Contains("0"))
                {
                    cont = false;
                }
            } while (cont);
        }

        public static string PrintSquare(int height, int length) {
            string output = "";
            for (int i = 0; i < height; i++)
            {
                if (i == 0)
                {
                    for (int k = 0; k < length; k++)
                    {
                        output += "#";
                    }
                    output += "\n";
                }
                else if (i == height - 1)
                {
                    for (int l = 0; l < length; l++)
                    {
                        output += "#";
                    }
                }
                else
                {
                    output += "#";
                    for (int j = 0; j < length - 2; j++)
                    {
                        output += " ";
                    }
                    output += "#\n";
                }
            }
            return output;
        }
    }
}
